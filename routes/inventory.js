var express = require('express');
var router = express.Router();
const model = require('../models/index');// GET users listing.

router.get('/', async function (req, res, next) {
  try {
    const invent = await model.inventory.findAll({});
    if (invent.length !== 0) {
      res.json({
        'status': 'OK',
        'messages': '',
        'data': invent
      })
    } else {
      res.json({
        'status': 'ERROR',
        'messages': 'EMPTY',
        'data': {}
      })
    }
  } catch (err) {
    res.json({
      'status': 'ERROR',
      'messages': err.messages,
      'data': {}
    })
  }
});

module.exports = router;
